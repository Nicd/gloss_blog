#!/usr/bin/env bash

set -euxo pipefail

gleam run
cp -r ./assets/* ./public
