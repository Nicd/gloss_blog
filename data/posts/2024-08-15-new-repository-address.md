New Repository Address
forgejo, git
time: 18:30 Europe/Helsinki
description: Scriptorium's Git repository has a new home

I've been lately moving stuff to my own little home server, and now Scriptorium's repository has also been moved. It's
now available as a [Forgejo](https://forgejo.org) instance at
[git.ahlcode.fi/nicd/scriptorium](https://git.ahlcode.fi/nicd/scriptorium). The GitLab repository has accordingly been
archived.

You can sign up to my instance by using a GitHub or GitLab account, so no real new account is needed (and
indeed you cannot register a regular account). When Forgejo enables federation features, you can hopefully contribute
from any Forgejo instance.

In case you don't want to make an account but have something to contribute, don't hesitate to contact me via other means
such as on the Gleam Discord server or [on the fediverse](https://social.ahlcode.fi/@nicd). Let's make it work. :)

This demo blog still resides on GitLab, as it uses GitLab Pages to be published. I don't have CI on my instance yet, so
this will have to be moved later.
