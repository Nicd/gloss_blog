Kirjoita omalla kielellä
i18n
time: 09:30 Europe/Helsinki
description: Scriptoriumiin lisätty tuki eri kielillä kirjoittamiseen.
lang: fi

Tähän mennessä Scriptoriumilla on voinut kirjoittaa vain yhdellä kielellä, joka on ollut
konfiguroitavissa globaalisti. Uusimmassa versiossa blogimoottoriin on lisätty tuki kielen
vaihtamiseen jokaiselle tekstille tai sivulle erikseen. Hurraa! Tämä tapahtuu asettamalla
postaukselle tai sivulle otsikkotieto `lang`, esimerkiksi <kbd>lang: fi</kbd>.

Tämän lisäksi koko generoidun blogin voi nyt kääntää Gettextillä. Jos valitset kieleksi
<kbd>"fi"</kbd>, sivuston kiinteät tekstit kuten <span lang="en">"Posted at"</span>
vaihtuvat suomeksi. Kielitiedostot voi kääntää ja ladata itse, tai ne voi lisätä
Scriptoriumin koodipohjaan jotta kaikki voivat hyötyä niistä.

<p lang="en">
  And the same in English: Up until now, Scriptorium has only supported one language for
  the entire blog. In the latest version (2.0.0), a header such as <kbd>lang: fi</kbd>
  can be added to change the language of a single post. This affects the generated HTML
  and the Atom feed.
</p>

<p lang="en">
  In addition, all the fixed strings in the generated site can now also be translated
  using Gettext. The user can do the translations themselves with their own translation
  files, or they can be contributed to the main project to be available for all.
</p>
