Scriptorium 1.0 Published
hex
time: 11:00 Europe/Helsinki
description: Announcing the release of Scriptorium

<figure>
  <blockquote>
    <p>
      scriptorium /skrĭp-tôr′ē-əm/<br>
      <br>
      Writing room set aside in monastic communities for the use of scribes engaged in copying manuscripts. […]
    </p>
  </blockquote>

  <figcaption>
    <cite>
      <a href="https://www.britannica.com/art/scriptorium">Encyclopædia Britannica</a>
    </cite>
  </figcaption>
</figure>

This morning I formally published [Scriptorium](https://hexdocs.pm/scriptorium) 1.0.0 (and 1.0.1, and
1.0.2 😅). The library now has API docs and a quickstart guide, but the full
[user's guide](/scriptorium_blog/guide.html) is still a work-in-progress. For now if there's any
prospective user, they can contact me directly for assistance (I'm Nicd in the Gleam Discord). There's
still surely some CSS and bug fixes to do, and some refactoring.

<!-- SPLIT -->

An observant reader may have noticed that the project used to be called "Gloss", which I still think is
a nice name. Sadly between the conception of the project and its finish, someone squatted the name in the
Hex.pm repository with a package that does nothing. I contacted Hex about it, but after two weeks I
decided that I'd rather just rename the project. So, scriptorium it is, then.
