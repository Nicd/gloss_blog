Version 2.0.1 with Deps Bumps
hex
time: 19:30 Europe/Helsinki
description: A patch update to bump deps in Scriptorium.

Just a small update to bump some dependencies that were preventing using Scriptorium on the latest Gleam
stdlib. This now requires the latest stdlib to work.
