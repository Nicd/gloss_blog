import gleam/io
import gleam/option
import gleam/result
import scriptorium/builder
import scriptorium/config.{type Configuration, Configuration}
import scriptorium/defaults

pub fn main() {
  let config =
    defaults.default_config(
      "Scriptorium Blog",
      "https://nicd.gitlab.io/scriptorium_blog",
      "en",
      config.Author(
        name: "Mikko Ahlroth",
        email: option.Some("mikko@ahlroth.fi"),
        url: option.Some("https://masto.ahlcode.fi/@nicd"),
      ),
      "© Mikko Ahlroth",
    )

  let config = Configuration(..config, output_path: "./public")

  io.debug(build(config))
}

pub fn build(config: Configuration) {
  use db <- result.try(builder.parse(config))
  let compiled = builder.compile(db, config)
  let posts = builder.render(db, compiled, config)
  builder.write(posts, config)
}
