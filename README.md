# scriptorium_blog

This is a demo blog for the [Scriptorium](https://hexdocs.pm/scriptorium) blog generator.

Type `gleam run` to generate the blog.
